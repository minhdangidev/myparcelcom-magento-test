/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Checkout/js/model/resource-url-manager',
    'Magento_Checkout/js/model/quote',
    'mage/storage',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/error-processor',
    'myparcelcom_checkout'
], function(resourceUrlManager, quote, storage, shippingService, rateRegistry, errorProcessor, mp_checkout) {
    'use strict';

    return {
        /**
         * @param {Object} address
         */
        getRates: function(address) {
            var cache;

            shippingService.isLoading(true);
            cache = rateRegistry.get(address.getKey());

            mpSelectedCC = address.countryId;
            mpSelectedPC = address.postcode;

            if (cache) {
                shippingService.setShippingRates(cache);
                shippingService.isLoading(false);
                /**
                 * MyParcel get first pickup location by shipping address
                 * */
                mp_checkout.setFirstDelivery(address, cache);
                mp_checkout.setFirstLocationByAddress(address, cache);
            } else {
                storage.post(
                    resourceUrlManager.getUrlForEstimationShippingMethodsByAddressId(),
                    JSON.stringify({
                        addressId: address.customerAddressId
                    }),
                    false
                ).done(function(result) {
                    rateRegistry.set(address.getKey(), result);
                    shippingService.setShippingRates(result);

                    /**
                     * MyParcel get first pickup location by shipping address
                     * */
                    mp_checkout.setFirstDelivery(address, cache);
                    mp_checkout.setFirstLocationByAddress(address, result);
                }).fail(function(response) {
                    shippingService.setShippingRates([]);
                    errorProcessor.process(response);
                }).always(function() {
                    shippingService.isLoading(false);
                });
            }
        }
    };
});