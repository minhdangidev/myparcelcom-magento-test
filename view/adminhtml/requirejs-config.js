var config = {
    "map": {
        "*": {
            "myparcelcom_massaction": "MyParcelCOM_Magento/js/mass-action",
            'myparcelcom_url_helper': 'MyParcelCOM_Magento/js/mp-url-helper',
            'myparcelcom_delivery_helper': 'MyParcelCOM_Magento/js/mp-delivery-helper',
            'myparcelcom_system_config': 'MyParcelCOM_Magento/js/mp-system-config'
        }
    }
};